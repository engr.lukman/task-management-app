import { yupResolver } from "@hookform/resolvers/yup";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import * as yup from "yup";

import { RoutePaths } from "constants/RouteNames";
import { AuthService } from "services/AuthService";
import { login, logout } from "store/slices/authSlice";

const LoginFeature = () => {
  const auth = useSelector((state: any) => state.auth);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const authSchema = yup.object({
    username: yup.string().required("Please enter your username."),
    password: yup.string().required("Please enter your password."),
  });

  const [error, setError] = useState<string | null>(null);
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);

  useEffect(() => {
    if (auth.isAuthenticated) {
      navigate(RoutePaths.DASHBOARD);
    } else {
      navigate(RoutePaths.LOGIN);
    }
  }, [auth.isAuthenticated]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      username: "",
      password: "",
    },
    resolver: yupResolver(authSchema),
  });

  const handleLogin = async (values: any) => {
    setIsSubmitting(true);

    const payload = {
      username: values.username,
      password: values.password,
    };

    const authService = new AuthService();
    const authInfo: any = await authService.login(payload);

    if (authInfo && parseInt(authInfo.code) === 1000) {
      dispatch(login(authInfo));
      setError(null);
    } else {
      dispatch(logout());
      setError(authInfo?.message);
    }

    setIsSubmitting(false);
  };

  return (
    <div className="w-full h-screen flex justify-center items-center bg-gray-400">
      <form onSubmit={handleSubmit((values: any) => handleLogin(values))}>
        <div className="flex flex-col items-center justify-center">
          <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
            <div className="text-red-400 text-md text-center rounded p-2">
              {error}
            </div>
            <div className="mb-4">
              <label
                htmlFor="username"
                className="uppercase text-sm text-gray-600 font-bold"
              >
                Username
                <input
                  id="username"
                  {...register("username")}
                  type="text"
                  className="w-full bg-gray-300 text-gray-900 mt-2 p-3"
                />
              </label>
              <p className="text-xs italic text-red-500">
                {errors?.username?.message}
              </p>
            </div>
            <div className="mb-6">
              <label
                htmlFor="password"
                className="uppercase text-sm text-gray-600 font-bold"
              >
                password
                <input
                  id="password"
                  {...register("password")}
                  type="password"
                  className="w-full bg-gray-300 text-gray-900 mt-2 p-3"
                />
              </label>
              <p className="text-xs italic text-red-500">
                {errors?.password?.message}
              </p>
            </div>
            <div className="flex items-center justify-center">
              <button
                type="submit"
                className="bg-[#00156A] text-gray-100 p-3 rounded-lg w-full"
                disabled={isSubmitting}
              >
                {isSubmitting ? "Please wait..." : "Sign In"}
              </button>
            </div>
            <p className="text-xs pt-2 italic">
              Username: lukman, Password: 123456
            </p>
          </div>
        </div>
      </form>
    </div>
  );
};

export default LoginFeature;
