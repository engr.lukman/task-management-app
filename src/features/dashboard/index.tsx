import { Link } from "react-router-dom";

import { APP_NAME } from "constants/AppConstant";
import { RoutePaths, RouteTexts } from "constants/RouteNames";

const DashboardFeature = () => {
  return (
    <div className="flex w-full min-h-[500px] justify-center">
      <div className="mt-10">
        <h3 className="text-center text-2xl font-semibold">
          Welcome to {APP_NAME}.
        </h3>
        <div className="flex w-full justify-center items-center mt-10 space-x-5 text-white font-semibold">
          <Link
            className="bg-[#3866FF] px-4 py-2 rounded w-24 text-center"
            to={RoutePaths.TASKS}
          >
            {RouteTexts.TASKS}
          </Link>
          <Link
            className="bg-[#3866FF] px-4 py-2 rounded w-24 text-center"
            to={RoutePaths.MEMBERS}
          >
            {RouteTexts.MEMBERS}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default DashboardFeature;
