import { RoutePaths } from "constants/RouteNames";
import { Link, useParams } from "react-router-dom";

import { EditIcon } from "components/Icons";
import Spinner from "components/Spinner";
import { useGetTaskDetailsQuery } from "services/TaskService";

const TaskDetailsFeature = () => {
  const { id: taskId }: any = useParams();

  const {
    data: taskDetails,
    isLoading,
    isError,
  } = useGetTaskDetailsQuery(taskId, {
    skip: !taskId,
  });
  const details: any = taskDetails?.data?.item ?? {};

  return (
    <div className="flex w-full min-h-[500px] items-start">
      <div className="flex w-full justify-center items-center">
        <div className="flex w-1/2 justify-center items-center font-semibold">
          {isError ? (
            <div className="flex justify-center items-center text-red-600 mt-10">
              An error occurred. Please retry.
            </div>
          ) : isLoading ? (
            <div className="flex justify-center items-center">
              <Spinner className="w-8 h-8 mr-1" /> Loading...
            </div>
          ) : (
            <div className="w-full">
              <h3 className="text-2xl font-semibold underline">Task details</h3>
              <h3 className="flex justify-between text-xl mt-4 font-semibold mb-4">
                <p className="flex">
                  <span>{details?.title}</span>
                  <Link
                    to={`${RoutePaths.ADD_TASK}?id=${details?._id}`}
                    className="flex w-9 h-9 bg-[#F2F5FF] rounded-xl justify-center items-center ml-2"
                  >
                    <EditIcon />
                  </Link>
                </p>
              </h3>
              <p className="text-sm italic">{details?.description}</p>
              <p className="text-sm text-gray-600">
                <Link
                  to={`${RoutePaths.MEMBERS}/${details?.member?._id}`}
                  className=""
                >
                  Assignt To: {details?.member?.name}
                </Link>
              </p>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default TaskDetailsFeature;
