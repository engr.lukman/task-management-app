import moment from "moment";
import { useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";

import { EditIcon, TrashIcon, ViewIcon } from "components/Icons";
import Spinner from "components/Spinner";
import { RoutePaths, RouteTexts } from "constants/RouteNames";
import { TaskList } from "models/Task";
import {
  useDeleteTaskMutation,
  useGetTaskListQuery,
} from "services/TaskService";

const TaskFeature = () => {
  const { data: taskList, isLoading, isError } = useGetTaskListQuery();
  const items = taskList?.data?.items ?? [];

  const [isDeletedModalShow, setIsDeletedModalShow] = useState<boolean>(false);
  const [deletedId, setDeletedId] = useState<string>("");

  const [deleteTask, { isLoading: isDeleting }] = useDeleteTaskMutation();

  const showDeletedModal = (id: string) => {
    setDeletedId(id);
    setIsDeletedModalShow(true);
  };

  const hideDeletedModal = () => {
    setDeletedId("");
    setIsDeletedModalShow(false);
  };

  const onSubmitHandler = async () => {
    const resp: any = await deleteTask(deletedId);

    if (parseInt(resp?.data?.code) === 1000) {
      toast.success(resp?.data?.message);
    } else {
      toast.error(resp?.data?.message);
    }

    hideDeletedModal();
  };

  return (
    <div className="flex w-full min-h-[500px] items-start">
      <div className="flex w-full justify-center items-center">
        <div className="flex w-2/3 justify-center items-center font-semibold">
          <table className="table-auto w-full">
            <caption>
              <h3 className="flex justify-between text-xl mt-4 font-semibold mb-4">
                <span>Task List</span>
                <Link
                  className="bg-[#3866FF] px-4 py-2 rounded w-48 text-center text-white text-base"
                  to={RoutePaths.ADD_TASK}
                >
                  {RouteTexts.ADD_TASK}
                </Link>
              </h3>
            </caption>
            <thead>
              <tr className="bg-[#00156A] text-left px-2">
                <th className="text-white py-2 pl-2">Title</th>
                <th className="text-white">Creation Date</th>
                <th className="text-white">Assignt To</th>
                <th className="text-right text-white pr-2">Action</th>
              </tr>
            </thead>
            <tbody>
              {isError ? (
                <tr className="text-center">
                  <td colSpan={4}>An error occurred. Please retry.</td>
                </tr>
              ) : isLoading ? (
                <tr>
                  <td colSpan={4}>
                    <div className="flex justify-center items-center">
                      <Spinner className="w-8 h-8 mr-1" /> Loading...
                    </div>
                  </td>
                </tr>
              ) : items && items.length > 0 ? (
                items?.map((item: TaskList) => (
                  <tr key={item?._id} className="border-b text-left">
                    <td className="py-1">
                      <Link
                        className="underline"
                        to={`${RoutePaths.TASKS}/${item?._id}`}
                      >
                        {item?.title?.length > 20
                          ? item?.title?.substring(20, 0).concat("...")
                          : item?.title}
                      </Link>
                    </td>
                    <td className="py-1">
                      {moment(item?.created_at).format("DD/MM/YYYY")}
                    </td>
                    <td className="py-1">
                      <Link
                        className="underline"
                        to={`${RoutePaths.MEMBERS}/${item?.member?._id}`}
                      >
                        {item?.member?.name}
                      </Link>
                    </td>
                    <td className="flex justify-end items-center space-x-2 py-1">
                      <Link
                        to={`${RoutePaths.TASKS}/${item?._id}`}
                        className="flex w-9 h-9 bg-[#F2F5FF] rounded-xl justify-center items-center ml-2"
                      >
                        <ViewIcon />
                      </Link>
                      <Link
                        to={`${RoutePaths.ADD_TASK}?id=${item?._id}`}
                        className="flex w-9 h-9 bg-[#F2F5FF] rounded-xl justify-center items-center"
                      >
                        <EditIcon />
                      </Link>
                      <button
                        onClick={() => showDeletedModal(item?._id)}
                        className="flex w-9 h-9 bg-[#F2F5FF] rounded-xl justify-center items-center"
                      >
                        <TrashIcon />
                      </button>
                    </td>
                  </tr>
                ))
              ) : (
                <tr className="text-center">
                  <td colSpan={4}>No data found.</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>

      {isDeletedModalShow && (
        <div className="absolute left-1/3 top-1/4 bg-gray-300 rounded w-1/4 h-48 z-10 p-4">
          <div>
            <h3 className="text-red-500 text-base text-center mt-5">
              Are you sure? <br />
              You want to delete this record?
              <div className="flex justify-center space-x-4 mt-10">
                <button
                  onClick={() => setIsDeletedModalShow(false)}
                  className="bg-red-500 px-2 py-1 rounded w-24 text-center text-white font-semibold"
                >
                  No
                </button>
                <button
                  onClick={() => onSubmitHandler()}
                  disabled={isDeleting}
                  className="bg-[#3866FF] px-2 py-1 rounded w-24 text-center text-white font-semibold"
                >
                  {isDeleting ? "Wait..." : "Yes"}
                </button>
              </div>
            </h3>
          </div>
        </div>
      )}
    </div>
  );
};

export default TaskFeature;
