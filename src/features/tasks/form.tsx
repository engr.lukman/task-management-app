import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import * as yup from "yup";

import { RoutePaths, RouteTexts } from "constants/RouteNames";
import { TaskPayload } from "models/Task";
import { useEffect } from "react";
import { useGetMemberListQuery } from "services/MemberService";
import {
  useCreateTaskMutation,
  useGetTaskDetailsQuery,
  useUpdateTaskMutation,
} from "services/TaskService";

const TaskFormFeature = () => {
  const navigate = useNavigate();
  const search = useLocation().search;
  const taskId: any = new URLSearchParams(search).get("id");

  const [createTask, { isLoading: isCreating }] = useCreateTaskMutation();
  const [updateTask, { isLoading: isUpdating }] = useUpdateTaskMutation();
  const { data: memberList, isLoading, isError } = useGetMemberListQuery(null);
  const members = memberList?.data?.items ?? [];

  const taskSchema = yup.object({
    title: yup.string().required("The title field is required."),
    description: yup.string(),
    member_id: yup.string(),
  });

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  }: any = useForm({ resolver: yupResolver(taskSchema) });

  const onSubmitHandler = async (values: any) => {
    let payload: TaskPayload = {
      title: values.title,
      description: values.description,
      member_id: values.member_id,
    };

    let resp: any;
    if (taskId) {
      payload.id = taskId;
      resp = await updateTask(payload);
    } else {
      resp = await createTask(payload);
    }

    if (parseInt(resp?.data?.code) === 1000) {
      toast.success(resp?.data?.message);
      navigate(RoutePaths.TASKS);
    } else {
      toast.error(resp?.data?.message);
    }
  };

  const { data: taskDetails } = useGetTaskDetailsQuery(taskId, {
    skip: !taskId,
  });
  useEffect(() => {
    if (taskDetails && taskDetails?.code == 1000) {
      const item: any = taskDetails?.data?.item;
      setValue("title", item?.title);
      setValue("description", item?.description);
      setValue("member_id", item?.member?._id);
    }
  }, [taskDetails]);

  return (
    <div className="flex w-full min-h-[500px] items-start">
      <div className="flex w-full justify-center items-center">
        <div className="flex w-1/2 justify-center items-center font-semibold">
          <form
            className="w-full"
            onSubmit={handleSubmit((values: any) => onSubmitHandler(values))}
          >
            <table className="table-auto w-full">
              <caption>
                <h3 className="flex justify-between text-xl mt-4 font-semibold mb-4">
                  <span>{taskId ? "Update Task" : RouteTexts.ADD_TASK}</span>
                </h3>
              </caption>
              <tbody>
                <tr>
                  <td>
                    <label className="text-sm text-gray-600 font-bold">
                      Title <span className="text-red-600">*</span>
                    </label>
                  </td>
                  <td>
                    <input
                      id="title"
                      {...register("title")}
                      type="text"
                      className="w-64 bg-gray-300 text-gray-900 p-2 m-2"
                      placeholder="Enter task title"
                    />
                    <p className="text-xs italic text-red-500 ml-2">
                      {errors?.title?.message}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign="top">
                    <label className="text-sm text-gray-600 font-bold">
                      Description
                    </label>
                  </td>
                  <td>
                    <textarea
                      id="description"
                      {...register("description")}
                      className="w-64 bg-gray-300 text-gray-900 p-2 ml-2"
                      placeholder="Enter task description"
                    />
                    <p className="text-xs italic text-red-500 m-2">
                      {errors?.description?.message}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td valign="top">
                    <label className="text-sm text-gray-600 font-bold">
                      Assignt To
                    </label>
                  </td>
                  <td>
                    <select
                      id="member_id"
                      {...register("member_id")}
                      className="w-64 bg-gray-300 text-gray-900 p-2 ml-2"
                    >
                      <option value="">---Select a member---</option>
                      {members &&
                        members?.length > 0 &&
                        members?.map((member) => (
                          <option value={member?._id}>{member?.name}</option>
                        ))}
                    </select>
                    <p className="text-xs italic text-red-500 m-2">
                      {errors?.member_id?.message}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                    <Link
                      className="bg-[#3866FF] px-4 py-2 rounded text-center text-white text-base ml-2"
                      to={RoutePaths.TASKS}
                    >
                      Back
                    </Link>
                    <button
                      type="submit"
                      className="bg-[#00156A] text-gray-100 p-2 rounded-lg w-32 m-2"
                      disabled={isCreating || isUpdating}
                    >
                      {isCreating || isUpdating ? "Please wait..." : "Submit"}
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </form>
        </div>
      </div>
    </div>
  );
};

export default TaskFormFeature;
