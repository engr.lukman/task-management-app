import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import * as yup from "yup";

import { RoutePaths, RouteTexts } from "constants/RouteNames";
import { MemberPayload } from "models/Member";
import { useEffect } from "react";
import {
  useCreateMemberMutation,
  useGetMemberDetailsQuery,
  useUpdateMemberMutation,
} from "services/MemberService";

const MemberFormFeature = () => {
  const navigate = useNavigate();
  const search = useLocation().search;
  const memberId: any = new URLSearchParams(search).get("id");

  const [createMember, { isLoading: isCreating }] = useCreateMemberMutation();
  const [updateMember, { isLoading: isUpdating }] = useUpdateMemberMutation();

  const memberSchema = yup.object({
    name: yup.string().required("The name field is required."),
    email: yup.string().email("Invalid email address"),
  });

  const {
    register,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  }: any = useForm({ resolver: yupResolver(memberSchema) });

  const onSubmitHandler = async (values: any) => {
    let payload: MemberPayload = {
      name: values.name,
      email: values.email,
    };

    let resp: any;
    if (memberId) {
      payload.id = memberId;
      resp = await updateMember(payload);
    } else {
      resp = await createMember(payload);
    }

    if (parseInt(resp?.data?.code) === 1000) {
      toast.success(resp?.data?.message);
      navigate(RoutePaths.MEMBERS);
    } else {
      toast.error(resp?.data?.message);
    }
  };

  const { data: memberDetails } = useGetMemberDetailsQuery(memberId, {
    skip: !memberId,
  });
  useEffect(() => {
    if (memberDetails && memberDetails?.code == 1000) {
      const item: any = memberDetails?.data?.item;
      setValue("name", item?.name);
      setValue("email", item?.email);
    }
  }, [memberDetails]);

  return (
    <div className="flex w-full min-h-[500px] items-start">
      <div className="flex w-full justify-center items-center">
        <div className="flex w-1/2 justify-center items-center font-semibold">
          <form
            className="w-full"
            onSubmit={handleSubmit((values: any) => onSubmitHandler(values))}
          >
            <table className="table-auto w-full">
              <caption>
                <h3 className="flex justify-between text-xl mt-4 font-semibold mb-4">
                  <span>
                    {memberId ? "Update Member" : RouteTexts.ADD_MEMBER}
                  </span>
                </h3>
              </caption>
              <tbody>
                <tr>
                  <td>
                    <label className="text-sm text-gray-600 font-bold">
                      Name <span className="text-red-600">*</span>
                    </label>
                  </td>
                  <td>
                    <input
                      id="name"
                      {...register("name")}
                      type="text"
                      className="w-64 bg-gray-300 text-gray-900 p-2 m-2"
                      placeholder="Enter member name"
                    />
                    <p className="text-xs italic text-red-500 ml-2">
                      {errors?.name?.message}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label className="text-sm text-gray-600 font-bold">
                      Email
                    </label>
                  </td>
                  <td>
                    <input
                      id="email"
                      {...register("email")}
                      type="text"
                      className="w-64 bg-gray-300 text-gray-900 p-2 ml-2"
                      placeholder="Enter member email"
                    />
                    <p className="text-xs italic text-red-500 m-2">
                      {errors?.email?.message}
                    </p>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>
                    <Link
                      className="bg-[#3866FF] px-4 py-2 rounded text-center text-white text-base ml-2"
                      to={RoutePaths.MEMBERS}
                    >
                      Back
                    </Link>
                    <button
                      type="submit"
                      className="bg-[#00156A] text-gray-100 p-2 rounded-lg w-32 m-2"
                      disabled={isCreating || isUpdating}
                    >
                      {isCreating || isUpdating ? "Please wait..." : "Submit"}
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </form>
        </div>
      </div>
    </div>
  );
};

export default MemberFormFeature;
