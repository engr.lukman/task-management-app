import { RoutePaths } from "constants/RouteNames";
import { Link, useParams } from "react-router-dom";

import { EditIcon } from "components/Icons";
import Spinner from "components/Spinner";
import { useGetMemberDetailsQuery } from "services/MemberService";

const MemberDetailsFeature = () => {
  const { id: memberId }: any = useParams();

  const {
    data: memberDetails,
    isLoading,
    isError,
  } = useGetMemberDetailsQuery(memberId, {
    skip: !memberId,
  });
  const details: any = memberDetails?.data?.item ?? {};

  return (
    <div className="flex w-full min-h-[500px] items-start">
      <div className="flex w-full justify-center items-center">
        <div className="flex w-1/2 justify-center items-center font-semibold">
          {isError ? (
            <div className="flex justify-center items-center text-red-600 mt-10">
              An error occurred. Please retry.
            </div>
          ) : isLoading ? (
            <div className="flex justify-center items-center">
              <Spinner className="w-8 h-8 mr-1" /> Loading...
            </div>
          ) : (
            <div className="w-full">
              <h3 className="text-2xl font-semibold underline">
                Member details
              </h3>
              <h3 className="flex justify-between text-xl mt-4 font-semibold mb-4">
                <p className="flex">
                  <span>{details?.name}</span>
                  <Link
                    to={`${RoutePaths.ADD_MEMBER}?id=${details?._id}`}
                    className="flex w-9 h-9 bg-[#F2F5FF] rounded-xl justify-center items-center ml-2"
                  >
                    <EditIcon />
                  </Link>
                </p>
              </h3>
              <h3 className="text-xl font-semibold mb-4">Assigned Tasks:</h3>
              <ul className="text-base">
                {details &&
                  details?.tasks?.length > 0 &&
                  details?.tasks?.map((task: any, key: number) => {
                    return (
                      <li
                        key={task?._id}
                        className={`p-2 text-white ${
                          key % 2 === 0 ? "bg-gray-400" : "bg-gray-500"
                        }`}
                      >
                        <h3 className="flex text-base items-center">
                          <Link
                            to={`${RoutePaths.TASKS}/${task?._id}`}
                            className="flex space-x-2 items-center underline"
                          >
                            <span>
                              {++key}. {task?.title}
                            </span>
                          </Link>
                        </h3>
                        <p className="text-sm italic">
                          {task?.description?.length > 75
                            ? task?.description?.substring(75, 0).concat("...")
                            : task?.description}
                        </p>
                      </li>
                    );
                  })}
              </ul>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default MemberDetailsFeature;
