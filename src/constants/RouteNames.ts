export enum RouteName {
  HOME = "HOME",
  LOGIN = "LOGIN",
  DASHBOARD = "DASHBOARD",
  MEMBERS = "MEMBERS",
  ADD_MEMBER = "ADD_MEMBER",
  DETAILS_MEMBER = "DETAILS_MEMBER",
  TASKS = "TASKS",
  ADD_TASK = "ADD_TASK",
  DETAILS_TASK = "DETAILS_TASK",
}

export const RoutePaths: { [x in RouteName]: string } = {
  HOME: "/",
  LOGIN: "/auth/login",
  DASHBOARD: "/dashboard",
  MEMBERS: "/members",
  ADD_MEMBER: "/members/form",
  DETAILS_MEMBER: "/members/:id",
  TASKS: "/tasks",
  ADD_TASK: "/tasks/form",
  DETAILS_TASK: "/tasks/:id",
};

export const RouteTexts: { [x in RouteName]: string } = {
  HOME: "Home",
  LOGIN: "Login",
  DASHBOARD: "Dashboard",
  MEMBERS: "Members",
  ADD_MEMBER: "Create New Member",
  DETAILS_MEMBER: "Member Details",
  TASKS: "Tasks",
  ADD_TASK: "Create New Task",
  DETAILS_TASK: "Task Details",
};

export const RouteMaps: { [x in typeof RoutePaths[RouteName]]: string } = {
  ...Object.keys(RoutePaths).reduce((v: any, k) => {
    v[RoutePaths[k as RouteName]] = RouteTexts[k as RouteName];
    return v;
  }, {}),
};
