import pkg from "../../package.json";

export const APP_VERSION = pkg.version;
export const APP_NAME = "Task Management";
export const AUTH = "login";
export const API_URL = "https://task-management-api.vercel.app/api/v1";
export const MAX_RETRIES = 3;
