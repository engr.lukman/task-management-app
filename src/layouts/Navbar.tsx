import { Link } from "react-router-dom";

import { RoutePaths, RouteTexts } from "constants/RouteNames";

export default function Navigation() {
  return (
    <nav>
      <ul className="flex space-x-5 mr-5 bg-[#00156A] p-3 text-white">
        <li>
          <Link to={RoutePaths.DASHBOARD}>{RouteTexts.DASHBOARD}</Link>
        </li>
        <li>
          <Link to={RoutePaths.TASKS}>{RouteTexts.TASKS}</Link>
        </li>
        <li>
          <Link to={RoutePaths.MEMBERS}>{RouteTexts.MEMBERS}</Link>
        </li>
      </ul>
    </nav>
  );
}
