import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { APP_NAME } from "constants/AppConstant";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Outlet } from "react-router-dom";
import { logout } from "store/slices/authSlice";

import avatar from "../assets/avatar.png";
import logo from "../assets/logo.svg";
import Navbar from "./Navbar";

const Layout = () => {
  const auth = useSelector((state: any) => state.auth);
  const username = auth?.user?.data?.username ?? "";
  const dispatch = useDispatch();
  const [isDropdownShow, setIsDropdownShow] = useState<boolean>(false);

  const Logout = () => {
    dispatch(logout());
  };

  useEffect(() => {
    setIsDropdownShow(false);
  }, [auth.isAuthenticated]);

  if (auth.isAuthenticated) {
    return (
      <>
        <div className="w-full flex justify-between items-center bg-[#00156A] space-x-2 p-2 h-20">
          <div className="flex items-center text-white">
            <img src={logo} className="app-logo" alt="logo" />
            <h3 className="text-2xl">{APP_NAME}</h3>
          </div>
          <div className="flex">
            <Navbar />
            <div className="w-10 h-10 p-1 rounded-full bg-white justify-center items-center pt-1">
              <img
                className="rounded-full cursor-pointer w-8 h-8"
                src={avatar}
                alt="Logout"
                onClick={() => setIsDropdownShow(!isDropdownShow)}
              />
              {isDropdownShow && (
                <div className="absolute w-64 h-auto bg-[#00156A] z-10 right-0 text-white font-semibold top-20">
                  <ul className="space-y-2">
                    <li className="flex justify-start items-center space-x-1 hover:bg-[#3866FF] px-4 py-2 uppercase">
                      <span>{username}</span>
                    </li>
                    <li
                      onClick={() => Logout()}
                      className="flex cursor-pointer justify-start items-center space-x-1 hover:bg-[#3866FF] px-4 py-2"
                    >
                      Logout
                    </li>
                  </ul>
                </div>
              )}
            </div>
          </div>
        </div>
        <Outlet />
        <div className="flex w-full justify-center items-center bg-[#00156A] p-2">
          <h1 className="text-center text-white">
            {new Date().getFullYear()} &copy; {APP_NAME}.
          </h1>
        </div>
        <ToastContainer />
      </>
    );
  }

  return (
    <>
      <Outlet />
    </>
  );
};

export default Layout;
