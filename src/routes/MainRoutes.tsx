import { BrowserRouter, Route, Routes } from "react-router-dom";

import { RoutePaths } from "constants/RouteNames";
import Layout from "layouts";
import LoginPage from "pages/auth";
import DashboardPage from "pages/dashboard";
import MemberPage from "pages/members";
import MemberDetailsPage from "pages/members/details";
import MemberFormPage from "pages/members/form";
import PageNotFound from "pages/PageNotFound";
import TaskPage from "pages/tasks";
import TaskDetailsPage from "pages/tasks/details";
import TaskFormPage from "pages/tasks/form";
import ProtectedRoute from "./ProtectedRoute";

export default function MainRoutes() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={RoutePaths.HOME} element={<Layout />}>
          <Route index element={<LoginPage />} />
          <Route path={RoutePaths.LOGIN} element={<LoginPage />} />

          <Route element={<ProtectedRoute />}>
            <Route path={RoutePaths.DASHBOARD} element={<DashboardPage />} />

            <Route path={RoutePaths.TASKS} element={<TaskPage />} />
            <Route
              path={RoutePaths.DETAILS_TASK}
              element={<TaskDetailsPage />}
            />
            <Route path={RoutePaths.ADD_TASK} element={<TaskFormPage />} />

            <Route path={RoutePaths.MEMBERS} element={<MemberPage />} />
            <Route
              path={RoutePaths.DETAILS_MEMBER}
              element={<MemberDetailsPage />}
            />
            <Route path={RoutePaths.ADD_MEMBER} element={<MemberFormPage />} />
          </Route>

          <Route path="*" element={<PageNotFound />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
