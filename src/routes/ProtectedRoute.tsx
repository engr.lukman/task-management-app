import { useSelector } from "react-redux";
import { Navigate, Outlet } from "react-router-dom";

import { RoutePaths } from "constants/RouteNames";

const ProtectedRoute = () => {
  const auth = useSelector((state: any) => state.auth);

  if (!auth.isAuthenticated) {
    return <Navigate to={RoutePaths.LOGIN} replace />;
  }

  return <Outlet />;
};

export default ProtectedRoute;
