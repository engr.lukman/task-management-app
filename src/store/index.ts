import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";

import MemberService from "services/MemberService";
import TaskService from "services/TaskService";
import ReducerNames from "store/ReducerNames";
import authReducer from "store/slices/authSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    [ReducerNames.MEMBERS]: MemberService.reducer,
    [ReducerNames.TASKS]: TaskService.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([
      MemberService.middleware,
      TaskService.middleware,
    ]),
  devTools: true,
});

setupListeners(store.dispatch);

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
