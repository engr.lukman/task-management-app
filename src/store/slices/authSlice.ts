import { createSlice } from "@reduxjs/toolkit";

import { AUTH } from "constants/AppConstant";
import { setStorage } from "helpers/Global";
import ReducerNames from "store/ReducerNames";

const initialState = {
  isAuthenticated: false,
  user: {},
};

export const authSlice = createSlice({
  name: ReducerNames.AUTH,
  initialState,
  reducers: {
    login: (state, action) => {
      state.isAuthenticated = true;
      state.user = action.payload;
      setStorage(AUTH, JSON.stringify(state.user));
    },
    logout: (state) => {
      state.isAuthenticated = false;
      state.user = {};
      setStorage(AUTH, JSON.stringify(state.user));
    },
  },
});

export const { login, logout } = authSlice.actions;
export default authSlice.reducer;
