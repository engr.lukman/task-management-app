enum ReducerNames {
  AUTH = "auth",
  MEMBERS = "members",
  TASKS = "tasks",
}

export default ReducerNames;
