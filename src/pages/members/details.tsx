import MemberDetailsFeature from "features/members/details";

const MemberDetailsPage = () => {
  return (
    <>
      <MemberDetailsFeature />
    </>
  );
};

export default MemberDetailsPage;
