import MemberFormFeature from "features/members/form";

const MemberFormPage = () => {
  return (
    <>
      <MemberFormFeature />
    </>
  );
};

export default MemberFormPage;
