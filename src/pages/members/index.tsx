import MemberFeature from "features/members";

const MemberPage = () => {
  return (
    <>
      <MemberFeature />
    </>
  );
};

export default MemberPage;
