import TaskFeature from "features/tasks";

const TaskPage = () => {
  return (
    <>
      <TaskFeature />
    </>
  );
};

export default TaskPage;
