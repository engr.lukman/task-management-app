import TaskDetailsFeature from "features/tasks/details";

const TaskDetailsPage = () => {
  return (
    <>
      <TaskDetailsFeature />
    </>
  );
};

export default TaskDetailsPage;
