import TaskFormFeature from "features/tasks/form";

const TaskFormPage = () => {
  return (
    <>
      <TaskFormFeature />
    </>
  );
};

export default TaskFormPage;
