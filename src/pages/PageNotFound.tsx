const PageNotFound = () => {
  return (
    <div className="flex w-full justify-center items-center text-red-500 font-semibold">
      <h1 className="mt-20">404 Page not found.</h1>
    </div>
  );
};

export default PageNotFound;
