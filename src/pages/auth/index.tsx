import LoginFeature from "features/auth";

const LoginPage = () => {
  return (
    <>
      <LoginFeature />
    </>
  );
};

export default LoginPage;
