import DashboardFeature from "features/dashboard";

const DashboardPage = () => {
  return (
    <>
      <DashboardFeature />
    </>
  );
};

export default DashboardPage;
