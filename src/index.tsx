import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";

import App from "./App";
import { AUTH } from "./constants/AppConstant";
import { getStorage } from "./helpers/Global";
import { store } from "./store";
import { login, logout } from "./store/slices/authSlice";

import "./styles/global.css";

const auth = getStorage(AUTH);
const checkAuth = auth ? JSON.parse(auth) : "";
if (checkAuth) {
  if (checkAuth?.data?.accessToken) {
    store.dispatch(login(checkAuth));
  } else {
    store.dispatch(logout());
  }
}

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);
