import { LoginPayload } from "models/Auth";
import { HttpClient } from "./axiosBaseQuery";

export class AuthService {
  client;

  constructor(baseUrl?: string) {
    this.client = new HttpClient(baseUrl);
  }

  public login = async (payload: LoginPayload) => {
    const resp: any = await this.client.request({
      url: "/auth/login",
      method: "POST",
      data: {
        username: payload?.username,
        password: payload?.password,
      },
    });

    return resp?.data;
  };
}
