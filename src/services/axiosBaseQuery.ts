import { BaseQueryFn } from "@reduxjs/toolkit/query";
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from "axios";

import { API_URL, MAX_RETRIES } from "constants/AppConstant";
import { getAccessToken } from "helpers/Global";

export type Response<T> = {
  status?: number;
  data: T;
  error?: {
    status: number;
    data: string;
  };
};

let accessToken = getAccessToken();

export class HttpClient {
  private axiosInstance: AxiosInstance;

  constructor(baseUrl: string = "") {
    this.axiosInstance = axios.create({
      baseURL: API_URL + baseUrl,
      timeout: 60000,
    });

    this.axiosInstance.interceptors.request.use(
      async (request: any) => {
        request.headers.common["Accept"] = `application/json`;
        request.headers.common[
          "Content-Type"
        ] = `application/json, multipart/form-data charset=UTF-8`;

        if (accessToken) {
          request.headers.common["Authorization"] = `Bearer ${accessToken}`;
        }

        return request;
      },
      (err) => {
        return Promise.reject({
          error: { status: err?.response?.status },
        });
      }
    );
  }

  public useResponseInterceptor = (
    success:
      | ((
          value: AxiosResponse<any, any>
        ) => AxiosResponse<any, any> | Promise<AxiosResponse<any, any>>)
      | undefined,
    reject?: ((error: any) => any) | undefined
  ) => {
    return this.axiosInstance.interceptors.response.use(success, reject);
  };

  async request(config: AxiosRequestConfig<any> = {}): Promise<Response<any>> {
    return new Promise((resolve, reject) => {
      this.axiosInstance
        .request(config)
        .then((resp) => resolve(resp))
        .catch((resp: Response<any>) => {
          reject(resp);
        });
    });
  }
}

const axiosBaseQuery =
  (
    { baseUrl }: { baseUrl: string } = { baseUrl: "" }
  ): BaseQueryFn<
    {
      url: string;
      method: AxiosRequestConfig["method"];
      data?: AxiosRequestConfig["data"];
      params?: AxiosRequestConfig["params"];
    },
    Response<any>,
    unknown
  > =>
  async ({ url, method, data, params }) => {
    const httpClient = new HttpClient();
    let retries: number = 0;

    const reFetch = async (): Promise<Response<any>> => {
      try {
        const result = await httpClient.request({
          url: baseUrl + url,
          method,
          data,
          params,
        });
        return { data: result.data };
      } catch (error) {
        let err = error as Response<any>;
        return err;
      }
    };

    httpClient.useResponseInterceptor(
      async (response: any) => {
        if (response && parseInt(response?.data?.code) === 4001) {
          if (retries >= MAX_RETRIES) {
            return Promise.reject(
              "Maximum retries reached. " + response?.data?.message
            );
          }

          retries++;

          if (accessToken) {
            const resp = await reFetch();
            if (!resp.error) {
              return Promise.resolve(resp);
            }

            return Promise.reject(resp);
          }
        }

        return response;
      },
      async (err) => {
        if (err?.response?.status === 401) {
          if (retries >= MAX_RETRIES) {
            return Promise.reject("Maximum retries reached. " + err.message);
          }

          if (accessToken) {
            retries++;
            const resp = await reFetch();
            if (!resp.error) {
              return Promise.resolve(resp);
            }
            return Promise.reject(resp);
          } else {
          }
        }
        return Promise.reject({
          error: { status: 500, data: err?.message },
        });
      }
    );

    return await reFetch();
  };

export default axiosBaseQuery;
