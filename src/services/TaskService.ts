import { createApi } from "@reduxjs/toolkit/query/react";

import { DefaultResponse } from "models/GlobalResponse";
import { TaskPayload } from "models/Task";
import ReducerNames from "store/ReducerNames";
import axiosBaseQuery from "./axiosBaseQuery";

const TASK_URL = "tasks";
const TASK_TAG = "Task";

const TaskService = createApi({
  reducerPath: ReducerNames.TASKS,
  baseQuery: axiosBaseQuery({
    baseUrl: "/",
  }),
  tagTypes: [TASK_TAG],
  endpoints: (builder) => ({
    getTaskList: builder.query<DefaultResponse<any>, void>({
      providesTags: [TASK_TAG],
      query: () => ({
        url: `${TASK_URL}`,
        method: "GET",
      }),
    }),
    getTaskDetails: builder.query<DefaultResponse<any>, string>({
      providesTags: [TASK_TAG],
      query: (id) => ({
        url: `${TASK_URL}/${id}`,
        method: "GET",
      }),
    }),
    createTask: builder.mutation<DefaultResponse<any>, TaskPayload>({
      query: (data) => ({
        url: `${TASK_URL}`,
        method: "POST",
        data,
      }),
      invalidatesTags: [TASK_TAG],
    }),
    updateTask: builder.mutation<DefaultResponse<any>, TaskPayload>({
      query: ({ id, ...rest }) => ({
        url: `${TASK_URL}/${id}`,
        method: "PUT",
        data: rest,
      }),
      invalidatesTags: [TASK_TAG],
    }),
    deleteTask: builder.mutation<DefaultResponse<any>, string>({
      query: (id) => ({
        url: `${TASK_URL}/${id}`,
        method: "DELETE",
      }),
      invalidatesTags: [TASK_TAG],
    }),
  }),
});

export const {
  useGetTaskListQuery,
  useGetTaskDetailsQuery,
  useCreateTaskMutation,
  useUpdateTaskMutation,
  useDeleteTaskMutation,
} = TaskService;
export default TaskService;
