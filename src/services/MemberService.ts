import { createApi } from "@reduxjs/toolkit/query/react";

import { DefaultResponse } from "models/GlobalResponse";
import { MemberPayload } from "models/Member";
import ReducerNames from "store/ReducerNames";
import axiosBaseQuery from "./axiosBaseQuery";

const MEMBER_URL = "members";
const MEMBER_TAG = "Member";

const MemberService = createApi({
  reducerPath: ReducerNames.MEMBERS,
  baseQuery: axiosBaseQuery({
    baseUrl: "/",
  }),
  tagTypes: [MEMBER_TAG],
  endpoints: (builder) => ({
    getMemberList: builder.query<DefaultResponse<any>, any>({
      providesTags: [MEMBER_TAG],
      query: () => ({
        url: `${MEMBER_URL}`,
        method: "GET",
      }),
    }),
    getMemberDetails: builder.query<DefaultResponse<any>, string>({
      providesTags: [MEMBER_TAG],
      query: (id) => ({
        url: `${MEMBER_URL}/${id}`,
        method: "GET",
      }),
    }),
    createMember: builder.mutation<DefaultResponse<any>, MemberPayload>({
      query: (data) => ({
        url: `${MEMBER_URL}`,
        method: "POST",
        data,
      }),
      invalidatesTags: [MEMBER_TAG],
    }),
    updateMember: builder.mutation<DefaultResponse<any>, MemberPayload>({
      query: ({ id, ...rest }) => ({
        url: `${MEMBER_URL}/${id}`,
        method: "PUT",
        data: rest,
      }),
      invalidatesTags: [MEMBER_TAG],
    }),
    deleteMember: builder.mutation<DefaultResponse<any>, string>({
      query: (id) => ({
        url: `${MEMBER_URL}/${id}`,
        method: "DELETE",
      }),
      invalidatesTags: [MEMBER_TAG],
    }),
  }),
});

export const {
  useGetMemberListQuery,
  useGetMemberDetailsQuery,
  useCreateMemberMutation,
  useUpdateMemberMutation,
  useDeleteMemberMutation,
} = MemberService;
export default MemberService;
