import { AUTH } from "constants/AppConstant";

export const setStorage = (keyName: string, data: any) => {
  return localStorage.setItem(keyName, data);
};

export const getStorage = (keyName: any) => {
  return localStorage.getItem(keyName);
};

export const removeStorage = (keyName: string) => {
  return localStorage.removeItem(keyName);
};

export const getAccessToken = () => {
  let accessToken = "";
  const auth = getStorage(AUTH);
  const checkAuth = auth ? JSON.parse(auth) : "";
  if (checkAuth) {
    if (checkAuth?.data?.accessToken) {
      accessToken = checkAuth?.data?.accessToken;
    }
  }

  return accessToken;
};
