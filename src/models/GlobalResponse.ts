export interface DefaultResponse<T> {
  code: number;
  message?: string;
  data?: {
    items?: T[];
    item?: T[];
    total?: number;
  };
}
