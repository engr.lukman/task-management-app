export interface TaskPayload {
  id?: string;
  title: string;
  description?: string;
  member_id?: string;
}

export interface TaskList {
  _id: string;
  title: string;
  description?: string;
  created_at: any;
  member?: {
    _id?: string;
    name: string;
    email?: string;
  };
}
