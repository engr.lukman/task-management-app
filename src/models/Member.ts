export interface MemberPayload {
  id?: string;
  name: string;
  email?: string;
}

export interface MemberList {
  _id: string;
  name: string;
  email?: string;
  tasks?: any[];
}
