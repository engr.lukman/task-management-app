# Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Tools and Technology

- React.js
- Redux Toolkit
- Typescript
- Tailwind CSS
- API
  - Express.js
  - Mongodb

## Demo Link

I have deployed the project to the vercel host.
[Demo link](https://task-management-app-theta.vercel.app) to view it in the browser.
